﻿using System;
using UnityEngine;

namespace BlockScalingTools {

    public class BlockScaleBox : MonoBehaviour {

        protected void OnMouseEnter() {
            scaleTool.ToolStateUpdate(MouseState.Enter);
        }

        protected void OnMouseExit() {
            scaleTool.ToolStateUpdate(MouseState.Exit);
        }

        protected void OnMouseDrag() {
            scaleTool.ToolStateUpdate(MouseState.Drag);
        }

        protected void OnMouseDown() {
            scaleTool.ToolStateUpdate(MouseState.Down);
        }

        protected void OnMouseUp() {
            scaleTool.ToolStateUpdate(MouseState.Up);
        }

        public BlockScaleTool scaleTool;

        public enum MouseState {
            Enter, Exit, Drag, Down, Up
        }
    }
}