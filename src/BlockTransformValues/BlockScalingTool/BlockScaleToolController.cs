﻿using Modding;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace BlockScalingTools {
    public class BlockScaleToolController : MonoBehaviour {

        GameObject root = null;
        GameObject translateButton = null;
        GameObject rotateButton = null;
        GameObject scaleButton = null;

        Vector3 rootPos;
        Vector3 translateButtonPos;
        Vector3 rotateButtonPos;
        Vector3 posOffset;

        Sprite scaleIcon;
        Mesh cube;

        public static BlockScaleToolController Instance { get; private set; }
        public static GameObject HandleInstance { get; private set; } = null;

        void Start() {
            DontDestroyOnLoad(gameObject);
            Instance = this;

            ReferenceMaster.onAdvancedBuildingToggled += () => ToggleAdvanced(StatMaster.advancedBuilding);

            Events.OnActiveSceneChanged += (x, y) => SpawnHandles();

            if (ModResource.AllResourcesLoaded) { SpawnHandles(); }
            else { ModResource.OnAllResourcesLoaded += SpawnHandles; }
        }

        public void ToggleAdvanced(bool isAdvanced) {
            if (!translateButton || !rotateButton || !scaleButton || !root) {
                return;
            }

            if (isAdvanced) {
                translateButton.transform.localPosition = translateButtonPos + posOffset;
                rotateButton.transform.localPosition = rotateButtonPos + posOffset;
                root.transform.localPosition = rootPos - posOffset * 0.5f;
            }
            else {
                translateButton.transform.localPosition = translateButtonPos;
                rotateButton.transform.localPosition = rotateButtonPos;
                root.transform.localPosition = rootPos;
            }
            scaleButton?.SetActive(isAdvanced);
        }

        void SpawnHandles() {
            if (Mod.SceneNotPlayable()) {
                return;
            }

            cube = cube
                ?? ModResource.GetMesh("cube");
            scaleIcon = scaleIcon
                ?? UIFactory.Texture2DToSprite(ModResource.GetTexture("ScaleIcon"));

            // scale button
            translateButton = GameObject.Find("HUD").transform.FindChild("TopBar/Mover/Align (Middle)/Buttons/Translate Tool").gameObject;
            rotateButton = GameObject.Find("HUD").transform.FindChild("TopBar/Mover/Align (Middle)/Buttons/Rotate Tool").gameObject;
            root = translateButton.transform.parent.gameObject;

            rootPos = root.transform.localPosition;
            translateButtonPos = translateButton.transform.localPosition;
            rotateButtonPos = rotateButton.transform.localPosition;
            posOffset = translateButtonPos - rotateButtonPos;

            TranslateButton trbOriginal = TranslateButton.Instance;
            scaleButton = Instantiate(translateButton, root.transform, false) as GameObject;
            scaleButton.name = "Scale Tool";
            scaleButton.transform.localPosition = rotateButton.transform.localPosition;
            scaleButton.transform.SetSiblingIndex(2);
            scaleButton.transform.FindChild("BG/Advanced options").gameObject.SetActive(true);
            DestroyImmediate(scaleButton.GetComponent<ScaleOnMouseOver>());
            DestroyImmediate(scaleButton.transform.FindChild("MiniGroundTool")?.gameObject);
            DestroyImmediate(scaleButton.transform.FindChild("Tooltip")?.gameObject);

            ScaleButton scb = scaleButton.AddComponent<ScaleButton>();
            TranslateButton trb = scaleButton.GetComponent<TranslateButton>();
            scb.globalButton = trb.globalButton;
            scb.pivotButton = trb.pivotButton;
            scb.linkedButton = trb.linkedButton;
            scb.snappingValue = trb.snappingValue;
            scb.toolControllerCode = MachineToolController.Instance;
            DestroyImmediate(trb);
            TranslateButton.Instance = trbOriginal;
            scb.globalButton.ResetDelegates();
            scb.pivotButton.ResetDelegates();
            scb.linkedButton.ResetDelegates();
            scb.snappingValue.ResetDelegate();

            Transform iconObject = scaleButton.transform.FindChild("Icon");
            iconObject.localScale = Vector3.one * 0.0935f;
            iconObject.GetComponent<SpriteRenderer>().sprite = scaleIcon;
            scaleButton.AddComponent<UIScaleOnMouseOver>().Target = iconObject;

            global::Tooltip t = scaleButton.GetComponent<global::Tooltip>();
            t.tooltipParent = scaleButton.transform.FindChild("AdvancedTooltip");
            t.lerpPosDirection = new Vector3(0, -0.5f, 0);
            t.Reset();
            FixTooltip(scaleButton);

            // handles
            HandleInstance = new GameObject("ScaleTool");
            HandleInstance.transform.parent = GameObject.Find("HUD").transform.FindChild("3D/BlockTransformParent").transform;

            //all: d:0.368,0.368,0.368, e:0.295,0.335,0.390
            Material normalMaterial = new Material(Shader.Find("Legacy Shaders/VertexLit"));
            normalMaterial.color = new Color(0.368f, 0.368f, 0.368f);
            normalMaterial.SetColor("_Emission", new Color(0.295f, 0.335f, 0.390f));
            normalMaterial.SetFloat("_Shininess", 0.7f);

            GameObject endCube = new GameObject("EndCube");
            endCube.AddComponent<MeshFilter>().mesh = cube;
            MeshRenderer mrn = endCube.AddComponent<MeshRenderer>();
            endCube.AddComponent<BlockScaleBox>();
            endCube.layer = 23;
            endCube.transform.localScale = new Vector3(0.41f, 0.41f, 0.41f);
            mrn.material = normalMaterial;

            //selected: d:0.191,0.191,0.191, e:0.463,0.377,0.017
            Material selectedMaterial = new Material(Shader.Find("Legacy Shaders/VertexLit"));
            selectedMaterial.color = new Color(0.191f, 0.191f, 0.191f);
            selectedMaterial.SetColor("_Emission", new Color(0.463f, 0.377f, 0.017f));
            selectedMaterial.SetFloat("_Shininess", 0.7f);

            HandleInstance.SetActive(false);

            GameObject all = new GameObject("ALL");
            all.transform.parent = HandleInstance.transform;
            endCube.transform.parent = all.transform;

            BlockScaleTool allHandle = all.AddComponent<BlockScaleTool>();
            allHandle.scaleAxis = BlockScaleTool.ScaleAxis.All;
            allHandle.toolTransform = HandleInstance.transform.parent;
            allHandle.objToMove = endCube.transform;
            allHandle.myRenderers = new Renderer[] { mrn };
            allHandle.DefaultMaterial = normalMaterial;
            allHandle.highlightMaterial = normalMaterial;
            allHandle.selectedMaterial = selectedMaterial;
            endCube.GetComponent<BlockScaleBox>().scaleTool = allHandle;
            endCube.AddComponent<BoxCollider>().size = new Vector3(1.4f, 1.4f, 1.4f);

            //x: d:0.294,0.294,0.294, e0:0.316,0.016,0.079, eS:0.441,0.104,0.175
            BlockScaleTool xHandle = NewTool(new Vector3(270, 270, 0), endCube, BlockScaleTool.ScaleAxis.X, "X", selectedMaterial,
                new Color(0.294f, 0.294f, 0.294f), new Color(0.316f, 0.016f, 0.079f), new Color(0.441f, 0.104f, 0.175f));
            //y: d:0.257,0.257,0.257, e0:0.172,0.309,0.043, eS:0.245,0.346,0.150
            BlockScaleTool yHandle = NewTool(new Vector3(0, 0, 0), endCube, BlockScaleTool.ScaleAxis.Y, "Y", selectedMaterial,
                new Color(0.257f, 0.257f, 0.257f), new Color(0.172f, 0.309f, 0.043f), new Color(0.245f, 0.346f, 0.150f));
            //z: d:0.309,0.309,0.309, e0:0.000,0.213,0.205, eS:0.083,0.257,0.251 
            BlockScaleTool zHandle = NewTool(new Vector3(270, 180, 0), endCube, BlockScaleTool.ScaleAxis.Z, "Z", selectedMaterial,
                new Color(0.309f, 0.309f, 0.309f), new Color(0.000f, 0.213f, 0.205f), new Color(0.083f, 0.257f, 0.251f));

            allHandle.scaleTools = new BlockScaleTool[] { xHandle, yHandle, zHandle };

            List<Transform> toolList = AdvancedBlockEditor.Instance.Tools.ToList();
            toolList.Insert((int)StatMaster.Tool.Scale, HandleInstance.transform);
            AdvancedBlockEditor.Instance.Tools = toolList.ToArray();
            AdvancedBlockEditor.allowScaleGizmo = true;
            HandleInstance.transform.localScale = Vector3.one;
            HandleInstance.transform.localPosition = Vector3.zero;
            BlockSelectionToolScale.Consume(AddPiece.Instance.selectionController);
        }

        BlockScaleTool NewTool(Vector3 rotation, GameObject endCube, BlockScaleTool.ScaleAxis axis,
            string name, Material selectedMaterial, Color diffuse,
            Color normal, Color highlight) {
            GameObject root = new GameObject(name);
            root.transform.parent = HandleInstance.transform;

            GameObject xEndCube = Instantiate(endCube);
            xEndCube.name = "EndCube";
            xEndCube.transform.parent = root.transform;
            xEndCube.transform.localPosition = new Vector3(0, 2, 0);
            xEndCube.transform.localScale = new Vector3(0.351f, 0.351f, 0.351f);

            Material normalMaterial = new Material(Shader.Find("Legacy Shaders/VertexLit"));
            normalMaterial.color = diffuse;
            normalMaterial.SetColor("_Emission", normal);
            normalMaterial.SetFloat("_Shininess", 0.7f);
            xEndCube.GetComponent<MeshRenderer>().material = normalMaterial;

            GameObject vis = Instantiate(endCube);
            vis.name = "Vis";
            vis.transform.parent = root.transform;
            vis.transform.localPosition = new Vector3(0, 1, 0);
            vis.transform.localScale = new Vector3(0.07878f, 2.0f, 0.07878f);
            vis.GetComponent<MeshRenderer>().material = normalMaterial;
            Destroy(vis.GetComponent<BoxCollider>());

            BlockScaleTool handle = root.AddComponent<BlockScaleTool>();
            handle.scaleAxis = axis;
            handle.toolTransform = HandleInstance.transform.parent;
            handle.objToMove = xEndCube.transform;
            handle.myRenderers = new Renderer[] { vis.GetComponent<MeshRenderer>(), xEndCube.GetComponent<MeshRenderer>() };
            handle.DefaultMaterial = normalMaterial;
            Material highlightMaterial = new Material(normalMaterial);
            highlightMaterial.SetColor("_Emission", highlight);
            handle.highlightMaterial = highlightMaterial;
            handle.selectedMaterial = selectedMaterial;
            xEndCube.GetComponent<BlockScaleBox>().scaleTool = handle;
            root.transform.localRotation = Quaternion.Euler(rotation);

            return handle;
        }

        public static void FixTooltip(GameObject go) {
            global::Tooltip t = go.GetComponentInChildren<global::Tooltip>();
            Dictionary<Renderer, Color> renOrgColors = new Dictionary<Renderer, Color>();
            foreach (Renderer r in t.renOrgColors.Keys) {
                Color c = t.renOrgColors[r];
                c.a = 1f;
                renOrgColors[r] = c;
            }
            t.renOrgColors = renOrgColors;
            Dictionary<GameObject, Color> textOrgColors = new Dictionary<GameObject, Color>();
            foreach (GameObject g in t.textOrgColors.Keys) {
                Color c = t.textOrgColors[g];
                c.a = 1f;
                textOrgColors[g] = c;
            }
            t.textOrgColors = textOrgColors;
        }
    }
}