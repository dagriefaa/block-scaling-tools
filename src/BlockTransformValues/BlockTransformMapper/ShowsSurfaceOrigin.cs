﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockScalingTools {
    internal class ShowsSurfaceOrigin : MonoBehaviour {

        MeshRenderer renderer;

        public static void Init() {
            Transform originalCoMVis = Resources.FindObjectsOfTypeAll<MachineCenterOfMass>().First().CenterOfMassVis;
            GameObject surfaceOriginVis = Instantiate(originalCoMVis.gameObject, originalCoMVis.parent, false) as GameObject;
            surfaceOriginVis.AddComponent<ShowsSurfaceOrigin>();
            surfaceOriginVis.SetActive(true);

            surfaceOriginVis.name = "SurfaceOrigin";
            foreach (Transform line in surfaceOriginVis.transform) {
                Destroy(line.gameObject);
            }
            surfaceOriginVis.transform.localScale = Vector3.one * 0.01f;
            surfaceOriginVis.AddComponent<ScaleRelativeToCamera>();
        }

        void Start() {
            renderer = GetComponent<MeshRenderer>();
        }

        void Update() {
            if (BlockMapper.IsOpen && BlockMapper.CurrentInstance.Block is BuildSurface) {
                renderer.enabled = true;
                this.transform.position = BlockMapper.CurrentInstance.Block.transform.position;
            }
            else {
                renderer.enabled = false;
            }
        }
    }
}
