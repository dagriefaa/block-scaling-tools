﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace BlockScalingTools {
    public class Vector3Selector : MonoBehaviour {

        Vector3 _value;
        public Vector3 Value {
            get { return _value; }
            set {
                _value = value;
                for (int i = 0; i < 3; i++) {
                    holders[i].Value = value[i];
                }
            }
        }
        public Vector3 InitialValue { get; private set; }

        Vector3 UnadjustedValue;

        Func<Vector3, Vector3, Component, Vector3> OnDragTransform = (v, i, c) => v;

        public Action<Vector3> OnValueChangeStart = null;
        public Action<Vector3> OnValueDragged = null;
        public Action<Vector3> OnValueChangeFinished = null;

        public enum Component { x, y, z }

        List<ValueHolderDrag> holders;

        /// <summary>
        /// Create a Vector3Selector.
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="offset"></param>
        /// <param name="icon"></param>
        /// <param name="iconScale"></param>
        /// <param name="initialValue"></param>
        /// <param name="suffix">Suffix for component values</param>
        /// <param name="dragMultiplier">Increase/decrease drag speed.</param>
        /// <param name="onDragTransform">(Vector3 current, Vector3 initial, Component active) => Vector3 result - 
        /// Returns a transformation on the raw input vector. Does nothing if null.</param>
        /// <param name="copyButton">Accommodates a button next to the icon.</param>
        /// <returns></returns>
        public static Vector3Selector Create(Transform parent, Vector3 offset, Sprite icon, float iconScale,
            Vector3 initialValue, string suffix = "", float dragMultiplier = 1,
            Func<Vector3, Vector3, Component, Vector3> onDragTransform = null,
            Transform copyButton = null) {

            Transform root = UIFactory.Empty(parent, offset, "Vector3Selector", layer: UIFactory.HUD_LAYER_KEYMAPPER);

            Transform iconTransform = UIFactory.Sprite(root, icon, new Vector3(-1.86f, 0), Vector3.one * iconScale, layer: UIFactory.HUD_LAYER_KEYMAPPER, material: UIConfig.ICON_MAT_STENCIL).transform;

            if (copyButton) {
                copyButton.SetParent(root, false);
                copyButton.localPosition = new Vector3(-1.69f, -0.1f);
                iconTransform.localPosition = new Vector3(-1.91f, 0);
            }

            Vector3Selector self = root.gameObject.AddComponent<Vector3Selector>();
            self.holders = new List<ValueHolderDrag>(){
                ValueHolderDrag.Create(root, new Vector3(-0.87f, 0), "X", 2083, initialValue.x, dragMultiplier, suffix),
                ValueHolderDrag.Create(root, new Vector3(0.36f, 0), "Y", 2084, initialValue.y, dragMultiplier, suffix),
                ValueHolderDrag.Create(root, new Vector3(1.56f, 0), "Z", 2085, initialValue.z, dragMultiplier, suffix)
            };

            if (onDragTransform != null) {
                self.OnDragTransform = onDragTransform;
            }

            return self;
        }

        void Start() {
            holders.ForEach(x => {
                x.OnValueChangeStarted = y => ValueChangeStarted();
                x.OnValueChangeFinished = v => ValueChangeFinished();
            });
            holders[(int)Component.x].OnValueDragged = v => ValueDragged(v, Component.x);
            holders[(int)Component.y].OnValueDragged = v => ValueDragged(v, Component.y);
            holders[(int)Component.z].OnValueDragged = v => ValueDragged(v, Component.z);

        }

        void ValueChangeStarted() {
            InitialValue = UnadjustedValue = Value;
            Debug.Log($"[{this.transform.GetSiblingIndex()}] ValueChangeStarted {Value}", this);
            if (OnValueChangeStart != null) {
                OnValueChangeStart(Value);
            }
        }

        void ValueDragged(float value, Component c) {
            UnadjustedValue[(int)c] = value;

            Value = OnDragTransform(UnadjustedValue, InitialValue, c);

            if (OnValueDragged != null) {
                OnValueDragged(Value);
            }
        }

        void ValueChangeFinished() {
            Value = new Vector3(holders[(int)Component.x].Value, holders[(int)Component.y].Value, holders[(int)Component.z].Value);
            UnadjustedValue = Value;
            Debug.Log($"[{this.transform.GetSiblingIndex()}] ValueChangeFinished {Value}", this);

            if (OnValueChangeFinished != null) {
                OnValueChangeFinished(Value);
            }
        }

        public void Flash() {
            holders.ForEach(x => x.Flash());
        }

        public void Lock(bool locked) {
            holders.ForEach(x => x.Locked = locked);
        }
    }
}