using BlockScalingTools;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlockScalingTools;
public class LevelTransformKeyListener : MonoBehaviour {

    TransformWidget widget = null;

    TransformSelector.TransformType ClipboardContents = TransformSelector.TransformType.None;
    Vector3 ClipboardTranslate = Vector3.zero;
    Vector3 ClipboardRotate = Vector3.zero;
    Vector3 ClipboardScale = Vector3.one;

    void Copy(TransformSelector.TransformType vector) {
        if (!widget) {
            widget = BlockMapper.CurrentInstance.GetComponentInChildren<TransformWidget>();
            if (!widget) {
                return;
            }
        }
        if (vector == TransformSelector.TransformType.All || vector == TransformSelector.TransformType.Translate) {
            ClipboardTranslate = new Vector3(widget.PosHolders[0].Value(), widget.PosHolders[1].Value(), widget.PosHolders[2].Value());
            Flash(widget.posFlashs);
        }
        if (vector == TransformSelector.TransformType.All || vector == TransformSelector.TransformType.Rotate) {
            ClipboardRotate = new Vector3(widget.RotHolders[0].Value(), widget.RotHolders[1].Value(), widget.RotHolders[2].Value());
            Flash(widget.rotFlashs);
        }
        if (vector == TransformSelector.TransformType.All || vector == TransformSelector.TransformType.Scale) {
            ClipboardScale = new Vector3(widget.ScaleHolders[0].Value(), widget.ScaleHolders[1].Value(), widget.ScaleHolders[2].Value());
            Flash(widget.scaleFlashs);
        }
        ClipboardContents = vector;
        Mod.ClickSound.Play();
        Debug.Log($"Copied {ClipboardContents} to T{ClipboardTranslate} R{ClipboardRotate} S{ClipboardScale}", this);
    }

    void PasteAll() {
        if (ClipboardContents == TransformSelector.TransformType.None) {
            return;
        }
        if (!widget) {
            widget = BlockMapper.CurrentInstance.GetComponentInChildren<TransformWidget>();
            if (!widget) {
                return;
            }
        }
        ReferenceMaster.Clipboard.position = BlockMapper.CurrentInstance.Entity.entity.Position;
        ReferenceMaster.Clipboard.euler = BlockMapper.CurrentInstance.Entity.entity.Rotation.eulerAngles;
        ReferenceMaster.Clipboard.scale = BlockMapper.CurrentInstance.Entity.entity.Scale;

        if (ClipboardContents == TransformSelector.TransformType.All || ClipboardContents == TransformSelector.TransformType.Translate) {
            ReferenceMaster.Clipboard.position = ClipboardTranslate;
            Flash(widget.posFlashs);
        }
        if (ClipboardContents == TransformSelector.TransformType.All || ClipboardContents == TransformSelector.TransformType.Rotate) {
            ReferenceMaster.Clipboard.euler = ClipboardRotate;
            Flash(widget.rotFlashs);
        }
        if (ClipboardContents == TransformSelector.TransformType.All || ClipboardContents == TransformSelector.TransformType.Scale) {
            ReferenceMaster.Clipboard.scale = ClipboardScale;
            Flash(widget.scaleFlashs);
        }
        widget.PasteAll();
        Debug.Log($"Pasted {ClipboardContents} to T{ClipboardTranslate} R{ClipboardRotate} S{ClipboardScale}", this);
    }


    void Update() {
        if (!StatMaster.isMP || !BlockMapper.IsOpen || !BlockMapper.CurrentInstance.IsEntity) {
            return;
        }
        if (!widget) {
            widget = BlockMapper.CurrentInstance.GetComponentInChildren<TransformWidget>();
        }

        if (TransformSelector.PasteKey.IsPressed) { PasteAll(); }
        else if (TransformSelector.CopyScaleKey.IsPressed) { Copy(TransformSelector.TransformType.Scale); }
        else if (TransformSelector.CopyPositionKey.IsPressed) { Copy(TransformSelector.TransformType.Translate); }
        else if (TransformSelector.CopyRotationKey.IsPressed) { Copy(TransformSelector.TransformType.Rotate); }
        else if (TransformSelector.CopyAllKey.IsPressed) { Copy(TransformSelector.TransformType.All); }
    }
    void Flash(MeshRenderer[] array) {
        StartCoroutine(FlashAnimation(array));
    }

    IEnumerator FlashAnimation(MeshRenderer[] array) {
        foreach (var r in array) {
            r.enabled = true;
        }
        for (float t = 0f; t < 0.25f; t += Time.unscaledDeltaTime) {
            float a = Mathf.Lerp(0.1f, 0f, t / .25f);
            foreach (var renderer in array) {
                renderer.material.SetColor("_TintColor", new Color(1, 1, 1, a));
            }
            yield return null;
        }
        foreach (var r in array) {
            r.enabled = false;
        }
    }
}