﻿using Modding.Blocks;
using ObjectExplorer.Mappings;
using System.Collections.Generic;
using UnityEngine;

namespace BlockScalingTools {

    internal class Mapper : MonoBehaviour {

        void Awake() {
            ObjectExplorer.ObjectExplorer.AddMappings("BlockScalingTools",
                new MVector3<BraceCode>("StartRotation",
                    c => c.startPoint.localEulerAngles,
                    (c, x) => BraceWarping.Warp(c, Quaternion.Euler(x), c.endPoint.localRotation)
                ),
                new MVector3<BraceCode>("EndRotation",
                    c => c.endPoint.localEulerAngles,
                    (c, x) => BraceWarping.Warp(c, c.startPoint.localRotation, Quaternion.Euler(x))
                ),

                new MBool<SurfaceSelector>(nameof(SurfaceSelector.IsFiltering), c => SurfaceSelector.IsFiltering),
                new MBool<SurfaceSelector>("TranformToolActive", c => Mod.BlockTransformToolActive())

            );

            Destroy(this);
        }
    }

}
