﻿using Besiege.UI;
using UnityEngine;

namespace BlockScalingTools {

    internal class RegistersHints : MonoBehaviour {

        void Start() {
            // tools
            HotkeyHints.Add("MULTI\nSELECT", "SHIFT", null, () => !Mod.TransformToolDragging()
                && (Mod.BlockTransformToolActive() || Mod.EntityTransformToolActive()));
            HotkeyHints.Add("FLIP GIZMO", "ALT", null, () => !InputManager.AdvancedBuilding.LeftShiftKey()
                && StatMaster.Mode.selectedTool != StatMaster.Tool.Mirror
                && (Mod.BlockTransformToolActive() || Mod.EntityTransformToolActive()));
            HotkeyHints.Add("NO SNAP", "CTRL", null, () => Mod.TransformToolDragging());

            // brace warping
            HotkeyHints.Add("(DRAG GIZMO)\nWARP BRACE", "CTRL", "D", () => BraceWarping.ShowHint() && !StatMaster.Mode.isRotating);
            HotkeyHints.Add("WARP BRACE", "CTRL", "D", () => BraceWarping.ShowHint() && StatMaster.Mode.isRotating);

            // surfaces
            HotkeyHints.Add("SELECT\nNODES ONLY", null, "P", Mod.BlockTransformToolActive);

            Destroy(this);
        }
    }
}
