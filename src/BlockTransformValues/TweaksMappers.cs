﻿using Modding;
using Selectors;
using UnityEngine;

namespace BlockScalingTools {

    /// <summary>
    /// Removes some restrictions on mappers.
    /// </summary>
    class TweaksMappers : MonoBehaviour {

        bool refresh = true;

        void Start() {
            Events.OnBlockInit += x => {
                x.InternalObject.MapperTypes.ForEach(y => {
                    if (y is MKey) {
                        (y as MKey).KeysChanged += () => refresh = true;
                    }
                    if (y is MText) {
                        (y as MText).TextChanged += t => refresh = true;
                    }
                });
            };
        }

        void Update() {
            if (!refresh || !BlockMapper.CurrentInstance) {
                return;
            }
            refresh = false;

            foreach (KeySelector k in BlockMapper.CurrentInstance.GetComponentsInChildren<KeySelector>()) {
                k.addButton.gameObject.SetActive(!k.Key.useMessage); // allow more than 3 keys to be added

                Transform textBox = k.transform.parent.FindChild("TextValueHolder");
                if (textBox) {
                    textBox.GetComponent<TextHolder>().CharLimit = int.MaxValue; // allow more than 20 chars
                }
            }
            foreach (TextSelector t in BlockMapper.CurrentInstance.GetComponentsInChildren<TextSelector>()) {
                Transform textBox = t.transform.parent.FindChild("TextValueHolder");
                if (textBox) {
                    textBox.GetComponent<TextHolder>().CharLimit = int.MaxValue; // allow more than 20 chars
                }
            }
        }
    }
}
