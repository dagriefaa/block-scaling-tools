﻿using System.Collections;
using UnityEngine;

namespace BlockScalingTools {
    public abstract class AbstractButton : ClickBehaviour {

        public Material InactiveMaterial = null;
        public Material ActiveMaterial = null;

        public Renderer renderer;

        /// <summary>
        /// Creates the button structure with background and content.
        /// Assumes that the content object has a center pivot.
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="content"></param>
        /// <param name="offset"></param>
        /// <param name="scale"></param>
        /// <param name="cOffset"></param>
        /// <param name="cScale"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        protected static GameObject CreateObject(Transform parent, Transform content, Vector3? offset = null, Vector3? scale = null,
            int layer = UIFactory.HUD_LAYER, string name = "Button") {

            GameObject self = new GameObject(name);
            self.layer = layer;
            self.transform.SetParent(parent, false);
            self.transform.localPosition = offset ?? Vector3.zero;
            self.transform.localScale = Vector3.one;

            Vector3 selfScale = scale ?? new Vector3(0.78f, 0.81f, 0.2f);

            UIFactory.Background(self.transform, new Vector3(0, 0, -0.4f), selfScale);
            BoxCollider collider = self.AddComponent<BoxCollider>();
            collider.size = selfScale;

            content.SetParent(self.transform, false);
            content.localPosition += new Vector3(0, 0, -0.5f);

            self.AddComponent<UIScaleOnMouseOver>().Target = content.transform;

            return self;
        }

        public virtual void UpdateMaterials(bool state, Material active = null, Material inactive = null) {
            if (!renderer) {
                renderer = this.transform.FindChild("BG").GetComponent<Renderer>();
            }

            if (active) {
                ActiveMaterial = active;
            }
            if (inactive) {
                InactiveMaterial = inactive;
            }
            if (!ActiveMaterial) {
                ActiveMaterial = UIConfig.BUTTON_ACTIVE_MAT;
            }
            if (!InactiveMaterial) {
                InactiveMaterial = UIConfig.BUTTON_INACTIVE_MAT;
            }

            renderer.enabled = true;
            renderer.sharedMaterial = (state) ? ActiveMaterial : InactiveMaterial;
        }
    }
}