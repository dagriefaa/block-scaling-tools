﻿using System.Collections;
using UnityEngine;

namespace BlockScalingTools {
    public class TextScaler : MonoBehaviour {

        DynamicText text;
        public float originalSize;
        public int maxChars;

        void Start() {
            text = GetComponent<DynamicText>();
            originalSize = text.size;
        }

        void Update() {
            // maxChars is multiplied by 1f so it doesn't do integer division
            text.size = originalSize * Mathf.Clamp((maxChars * 1f) / text.GetText().Length, 0.001f, 1);
        }
    }
}