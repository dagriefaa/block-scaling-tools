﻿using System;
using UnityEngine;

namespace BlockScalingTools {

    public class UndoActionBraceWarp : UndoAction {

        protected Quaternion startRotation;
        protected Quaternion endRotation;
        protected Quaternion oldStartRotation;
        protected Quaternion oldEndRotation;

        public UndoActionBraceWarp(Machine m, Guid blockGuid, Quaternion startRotation, Quaternion endRotation, Quaternion oldStartRotation, Quaternion oldEndRotation) {
            this.startRotation = startRotation;
            this.endRotation = endRotation;
            this.oldStartRotation = oldStartRotation;
            this.oldEndRotation = oldEndRotation;
            this.guid = blockGuid;
            this.changesTransform = true;
            this.machine = m;
        }

        public override bool Redo() {
            this.machine.GetBlock(guid, out BlockBehaviour block);

            BraceCode brace = block as BraceCode;
            if (!brace) return false;

            BraceWarping.Warp(brace, startRotation, endRotation);
            
            return true;
        }

        public override bool Undo() {
            this.machine.GetBlock(guid, out BlockBehaviour block);

            BraceCode brace = block as BraceCode;
            if (!brace) return false;

            BraceWarping.Warp(brace, oldStartRotation, oldEndRotation);

            return true;
        }
    }
}

