﻿using Modding;
using Modding.Blocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BlockScalingTools {
    public class UndoActionMove : global::UndoActionMove {

        public UndoActionMove(Machine m, Guid blockGuid, Vector3 p, Vector3 lPos) : base(m, blockGuid, p, lPos) {
        }

        public override bool Undo() {
            base.Undo();
            this.machine.GetBlock(guid, out BlockBehaviour block);
            if (block is BuildSurface) {
                (block as BuildSurface).UpdateSurface();
                ModNetworking.SendToAll(Messages.UpdateSurface.CreateMessage(new[] { Block.From(guid) }));
            }
            return true;
        }

        public override bool Redo() {
            base.Redo();
            this.machine.GetBlock(guid, out BlockBehaviour block);
            if (block is BuildSurface) {
                (block as BuildSurface).UpdateSurface();
                ModNetworking.SendToAll(Messages.UpdateSurface.CreateMessage(new[] { Block.From(guid) }));
            }
            return true;

        }
    }
}
